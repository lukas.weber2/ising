import loadleveller.mcextract as mce
import numpy as np
import matplotlib.pyplot as plt

# Demonstration of the mcextract module for easy access to simulation results.

mc = mce.MCArchive('job.results.json')

Ls = mc.get_parameter('Lx', unique=True)

# This is a common pattern. You want multiple datasets in the same plot
# labelled by a parameter (here Lx).
def plot_observable_scaling(name, label, autocorrelation=False):
    for L in Ls:
        # Then you can use the filter argument to only get the parameters
        # and data for one Lx

        cond = {'Lx':L}

        Ts = mc.get_parameter('T', filter=cond)
        obs = mc.get_observable(name, filter=cond)
        if not autocorrelation:
            plt.errorbar(Ts, obs.mean, obs.error, fmt='.-', label='L = {}'.format(L))
        else:
            plt.plot(Ts, obs.autocorrelation_time, '.-', label='L = {}'.format(L))
    
    plt.legend()
    plt.xlabel('$T$')
    plt.ylabel(label)
    plt.show()

def plot_corr(Ts, name, label, xlabel):
    L = max(Ls)
    cond = {'Lx':L}

    for i, T in enumerate(Ts):
        corr = mc.get_observable(name, filter={'Lx':L, 'T':T})
        idx = np.arange(corr.mean.shape[1])

        plt.errorbar(idx, corr.mean[0,:], corr.error[0,:], fmt='.-', label='T = {:.2g}'.format(T))
    plt.legend()
    plt.xlabel(xlabel)
    plt.ylabel(label)
    plt.show()
        

plot_observable_scaling('Magnetization2', '$m^2$')
plot_observable_scaling('Magnetization2', '$m^2$ autocorrelation time', autocorrelation=True)

plot_observable_scaling('Susceptibility', '$\chi$')

Ts = mc.get_parameter('T', unique=True)
Ts_reduced = [Ts[0], Ts[np.argmin(np.abs(Ts-2.27))], Ts[-1]]

plot_corr(Ts_reduced, 'SpinCorrelation', '$C(r)$', '$r$')
plot_corr(Ts_reduced, 'SpinCorrelationK', '$C(k)$', '$k$')

# loadleveller automatically measures timing data. This is useful for
# quick performance tests, but be careful as these naive timings may be
# affected by the environment you are running in.

for L in Ls:
    cond = {'Lx':L}

    Ts = mc.get_parameter('T', filter=cond)
    mtime = mc.get_observable('_ll_measurement_time', filter=cond)
    stime = mc.get_observable('_ll_sweep_time', filter=cond)
    plt.errorbar(Ts, mtime.mean*1e6/L**2, mtime.error*1e6/L**2, label='measurement L = {}'.format(L))
    plt.errorbar(Ts, stime.mean*1e6/L**2, stime.error*1e6/L**2, label='sweep L = {}'.format(L))

plt.legend()
plt.xlabel('$T$')
plt.ylabel('Runtime/$L^2$/$\mu$s')
plt.show()
