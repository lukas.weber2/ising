#include "ising.h"

ising::ising(const loadl::parser &p) : loadl::mc(p) {
	Lx = param.get<int>("Lx");
	Ly = param.get<int>("Ly", Lx);
	T = param.get<double>("T");
	spin.resize(Lx * Ly);
	correlation_buf.resize(Lx);
}

void ising::do_update() {
	for(uint n = 0; n < spin.size(); ++n) {
		int i = (int)(random01() * spin.size());
		int x = i % Lx;
		int y = i / Lx;
		int j1 = (x + 1) % Lx + y * Lx;        // right neighbor
		int j2 = x + ((y + 1) % Ly) * Lx;      // upper neighbor
		int j3 = (x - 1 + Lx) % Lx + y * Lx;   // left  neighbor
		int j4 = x + ((y - 1 + Ly) % Ly) * Lx; // lower neighbor
		double ratio = exp(-2. / T * spin[i] * (spin[j1] + spin[j2] + spin[j3] + spin[j4]));
		if(ratio >= 1 || ratio > random01())
			spin[i] = -spin[i];
	}
}

void ising::do_measurement() {
	double mag = 0;
	double energy = 0;

	std::fill(correlation_buf.begin(), correlation_buf.end(), 0);

	for(size_t i = 0; i < spin.size(); ++i) {
		int x = i % Lx;
		int y = i / Lx;
		int j1 = (x + 1) % Lx + y * Lx;   // right neighbor
		int j2 = x + ((y + 1) % Ly) * Lx; // upper neighbor
		mag += spin[i];
		energy += -spin[i] * (spin[j1] + spin[j2]);

		// smart people use even more lattice symmetries!
		correlation_buf[x] += spin[Lx * y] * spin[i];
	}

	for(auto &c : correlation_buf) {
		c /= Ly;
	}

	mag /= spin.size();
	energy /= spin.size();

	measure.add("Energy", energy);
	measure.add("Magnetization", mag);
	measure.add("AbsMagnetization", fabs(mag));
	measure.add("Magnetization2", mag * mag);
	measure.add("Magnetization4", mag * mag * mag * mag);

	// you can also do vector valued measurements
	measure.add("SpinCorrelation", correlation_buf);
}

void ising::init() {
	for(uint i = 0; i < spin.size(); ++i)
		spin[i] = (random01() < 0.5) ? -1 : 1;

	// if you want to explicitly set the binsize of your observables,
	// you can register them explicitly. Otherwise they will have the
	// given in the parameter file which is fine in most cases.
	size_t magic_binsize = 40;
	measure.register_observable("Energy", magic_binsize);
}

void ising::checkpoint_write(const loadl::iodump::group &d) {
	d.write("spins", spin);
}

void ising::checkpoint_read(const loadl::iodump::group &d) {
	d.read("spins", spin);
}

void ising::register_evalables(loadl::evaluator &eval, const loadl::parser &p) {
	int Lx = p.get<int>("Lx");
	int Ly = p.get<int>("Ly", Lx);
	double T = p.get<double>("T");

	eval.evaluate("BinderRatio", {"Magnetization2", "Magnetization4"},
	              [](const std::vector<std::vector<double>> &obs) {
		              // Internally, all observables are vectors so we get a vector of
		              // vectors.
		              double mag2 = obs[0][0];
		              double mag4 = obs[1][0];

		              // and even for scalar evalables, we need to return them as a vector.
		              return std::vector{mag2 * mag2 / mag4};
	              });

	eval.evaluate("Susceptibility", {"Magnetization2"},
	              [&](const std::vector<std::vector<double>> &obs) {
		              double mag2 = obs[0][0];
		              double chi = Lx*Ly * mag2 / T;

		              return std::vector{chi};
	              });

	eval.evaluate("SpinCorrelationK", {"SpinCorrelation"},
	              [](const std::vector<std::vector<double>> &obs) {
		              auto &corr = obs[0];
		              std::vector<double> corrk(corr.size());

		              // in practice you would use an FFT!
		              for(size_t i = 0; i < corr.size(); i++) {
			              for(size_t j = 0; j < corr.size(); j++) {
				              corrk[i] += corr[j] * cos(2 * M_PI / corr.size() * i * j);
			              }
		              }
		              return corrk;
	              });
}

void ising::write_output(const std::string & /*unique_filename*/) {
	// if you want you can output stuff here once per task. unique_filename is something like
	// jobname.taskname and will not collide with other tasks. but you will rarely need this
	// function. I never did.
}

// Parallel tempering section
//
// Parallel tempering is a method for exchanging configurations between different parameter sets.
// This can help the ergodicity of the simulation, but it can be slow and tricky so if you don’t
// need it, just ignore this section.

// To compute the acceptance probability of a configuration swap, loadleveller needs to know the
// log ratio of MC weights: log(weight(T_new)/weight(T_old)). You need to calculate it in this
// function. Why the log? The raw weight ratios are so big/small it would lead to rounding errors.
double ising::pt_weight_ratio(const std::string &param_name, double new_param) {
	// we only implement temperature swaps
	if(param_name != "T") {
		return 0;
	}

	double energy{};
	for(size_t i = 0; i < spin.size(); ++i) {
		int x = i % Lx;
		int y = i / Lx;
		int jr = (x + 1) % Lx + y * Lx;
		int ju = x + ((y + 1) % Ly) * Lx;
		energy += -spin[i] * (spin[jr] + spin[ju]);
	}

	return -(1 / new_param - 1 / T) * energy;
}

// Once an update succeeds the parameters are switched. loadleveller cannot do this for you
// because you might have saved it locally. Update your stuff here.
void ising::pt_update_param(const std::string &param_name, double new_param) {
	if(param_name == "T") {
		T = new_param;
	}
}
