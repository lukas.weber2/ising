#!/bin/bash

if [[ $# != 3 ]]; then
    echo "Usage: $0 <compiler> <srcdir> <builddir>"
    echo "Values for <compiler> can be 'gcc' or 'intel'"
    exit 1
fi

compiler=$1
srcdir=$2
builddir=$3

module unload gcc
module unload intel

if [ $compiler = "gcc" ]; then
    echo "Build using gcc"

    module load gcc/9
    CC=$GCC_ROOT/bin/gcc
    CXX=$GCC_ROOT/bin/g++

    module load LIBRARIES
    module load hdf5/1.10.4
    
    meson $srcdir $builddir -Dcpp_link_args="$FLAGS_FAST $FLAGS_HDF5_LINKER $FLAGS_HDF5_RPATH -g -static-libgcc -static-libstdc++" -Dcpp_args="$FLAGS_FAST $FLAGS_HDF5_INCLUDE -g"
elif [ $compiler = "intel" ]; then
    echo "Build using intel compiler"
    module load gcc/8
    module load intel/19.0

    $CC --version
    
    args="$FLAGS_FAST -g"
    pushd $srcdir
    meson wrap promote subprojects/loadleveller/subprojects/hdf5.wrap
    popd

    meson $srcdir $builddir -Dcpp_link_args="$args -static-libgcc -static-libstdc++ $gcc_link" -Dcpp_args="$args" -Dc_args="$args" -Dc_link_args="$args -static-libgcc" -Dloadleveller:force_hdf5_fallback=true
    echo -e "\n\nATTENTION: before building, type 'module load gcc/8' or else the right libstdc++ will not be found. The build will still use intel icpc ;)"
else
    echo "Unknown compiler '$compiler'"
fi

echo -e "\nWhen compiling with ninja on login nodes, keep in mind that by default it will spam lots of workers. If you want to be nice, use 'ninja -j1' and wait a bit longer."
