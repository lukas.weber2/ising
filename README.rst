Ising example code
==================

This is an example code for the loadleveller library simulating the Ising model using simple local Metropolis updates.

See `Tutorial <TUTORIAL.rst>`_ for a step-by-step guide from getting this code running from scratch until the first plot of your results.

The ``examples/`` directory contains annotated configuration files and scripts highlighting the features of loadleveller.
