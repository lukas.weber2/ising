Ising Tutorial
==============

In this Tutorial, we will see how to set up loadleveller, build this example code for the Ising model,
run it and evaluate the result in the end. Where possible, links will be added for the computationally inexperienced physicist to read up more on the technical foundations (if interested). This guide is taylored to the situation at the RWTH Aachen physics department.

0. Preparation
--------------

We will be installing python packages locally for this tutorial, which contain some executable scripts. Therefore, we need to make sure the path
::

    ~/.local/bin

is part of the `$PATH environment variable <https://en.wikipedia.org/wiki/PATH_(variable)>`_. The way to set them depends on the shell you are using. To find out which one it is do
::

    echo $0

which will most likely say either something like ``zsh`` or ``bash``.

- **bash**: Run ``echo 'export PATH="$HOME/.local/bin:$PATH"' >> ~/.bashrc``
- **zsh**: Run ``echo 'export PATH="$HOME/.local/bin:$PATH"' >> ~/.zshrc``

To continue without restarting the terminal, apply the change to the currently running shell via
::

    source ~/.bashrc

or similar for zsh.

1. Installing the Python Package
--------------------------------

This step is simple. Run
::

    pip3 install --user 'git+https://git.rwth-aachen.de/lukas.weber2/load_leveller.git#egg=loadleveller&subdirectory=python'

Now the python part of loadleveller is installed.

Loadleveller uses the `Meson <https://mesonbuild.com/>`_ `build system <https://medium.com/@julienjorge/an-overview-of-build-systems-mostly-for-c-projects-ac9931494444>`_ which is not installed on our machines. But it can also be installed via pip (or your packet manager if you are using your own device).
::

    pip3 install --user ninja meson

2. Building Ising
-----------------

To get the source code
::

    git clone https://git.rwth-aachen.de/lukas.weber2/ising.git

The nice thing about Meson is that it will do everything for you. Go to the directory containing ``ising.cpp`` and run
::
    
    meson . build
    cd build
    ninja

If this completes without error, the binary ``ising`` will appear in this directory and you are all set to run the code.

3. Your First Job
-----------------

Choose a nice directory to run your first simulation and copy the example jobfiles there.
::

    cd ..
    cp examples/* ~/some/nice/directory
    cd ~/some/nice/directory

Don’t worry about the ``parallel_tempering`` directory.

Open your favourite editor to take a look at the files ``job`` and ``jobconfig.yml``. (``job`` is just an executable python script that uses the ``loadleveller.taskmaker`` module to generate a more verbose job script in json format. Try running it by itself.) They contain the parameters describing what you will be simulating and how it will be run. In the ``jobconfig.yml`` file you need to change ``mc_binary`` to the path of the ``ising`` binary you built before. Then you can generate the jobfile and start the code using
::

    loadl run job

which will run the simulation in parallel with the amount of cores specified in the jobconfig. After it is done, you can view the results with the example script ``eval.py``. Play around with the script to plot other observables. Take a look at the ``job.results.json`` file to see what it looks like. Change some job parameters in ``job``. After such a change you generally have to delete the data from the previous run.
::

    loadl delete job
    loadl run job

or shorter 
::

    loadl run job --restart

Kill it before it is done to test something else: You can get results even if the run is unfinished. Check the progress of your unfinished run.
::

    loadl status job

To obtain postprocessed means and errors for everything that is done, you have to merge the job manually
::

    loadl merge job

then you can proceed with ``eval.py`` like before.

In practice, you should keep raw data and plotting/analysis scripts tidy and separated. For example, run jobs on a cluster, move the ``.results.json`` file to your local machine and run further analysis scripts there. You should also give your jobs more descriptive names than *job*.
The more parts of this process you can `efficiently automate <https://xkcd.com/1205/>`_ the better.

4. Understanding the Code
-------------------------

Go back to the source directory and take a look at ``ising.cpp`` and ``ising.h``. They contain the different callbacks for updates and measurements. For your own project you can use this as a base.

For practical work, it is useful to know some meson (or more general development) tricks.
::

    meson configure

gives you the current build settings. The buildtype is *release*. When debugging code you should switch it to debug or debugoptimized. This will switch on compiler flags like ``-g``
::

    meson configure -D buildtype=debug

If you have trouble with segfaults or memory leaks you can switch on the very powerful (and very slow) `undefined behavior and/or address sanitizers <https://gcc.gnu.org/onlinedocs/gcc/Instrumentation-Options.html>`_. An executable built with these will detect segfaults and memory leaks and print nice error messages.
::

    meson configure -D b_sanitize=address,undefined

For more info, see `<https://mesonbuild.com/>`_.

5. Information for the RWTH Cluster
-----------------------------------

On the RWTH cluster, building loadleveller can be a bit more complicated. This repository includes a script for setting up the proper meson build directory. Once everything is running, the ``loadl`` tool will automatically launch your jobs as batch jobs.

The standard version of python3 at this time is also too low, so you may need to use
::

    module load python/3.7.3
    unset PYTHONPATH

and put it in your .zshrc if you keep forgetting to call it every time you call ninja…
