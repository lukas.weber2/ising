#pragma once

#include <loadleveller/loadleveller.h>
#include <string>
#include <vector>

class ising : public loadl::mc {
private:
	int Lx;
	int Ly;
	double T;
	std::vector<int> spin;

	std::vector<double> correlation_buf;

public:
	void init();
	void do_update();
	void do_measurement();
	void checkpoint_write(const loadl::iodump::group &out);
	void checkpoint_read(const loadl::iodump::group &in);

	static void register_evalables(loadl::evaluator &evalables, const loadl::parser &p);
	void write_output(const std::string &unique_filename);

	double pt_weight_ratio(const std::string &param_name, double new_param);
	void pt_update_param(const std::string &param_name, double new_param);

	ising(const loadl::parser &p);
};
